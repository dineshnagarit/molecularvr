﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class SelectAndMoveObject : MonoBehaviour {

    public GameObject selectedObject;

    public GameObject hygrogenElement;
    public GameObject oxygenElement;
    public GameObject carbonElement;
    public GameObject sodiumElement;
    public GameObject flourineElement;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 mousePos = Input.mousePosition;
            Camera  c = Camera.main;
            var worldPos = c.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, c.farClipPlane));
            Vector3 fwd = transform.TransformDirection(worldPos);
            RaycastHit hit;
            Physics.Raycast(transform.position, fwd, out hit, 100000);
            Debug.DrawRay(transform.position, fwd, Color.green, 2);
            if (hit.collider != null)
            {
                //Add forceInstant to the gameObject.
                selectedObject = hit.collider.gameObject;
                var ifi = GetComponent<IndexFingerInteraction>();

                if (selectedObject == hygrogenElement || selectedObject == oxygenElement  || selectedObject == carbonElement|| selectedObject == sodiumElement|| selectedObject == flourineElement)
                {
                    ifi.createAtom(selectedObject, ifi._atomPrefab, ifi._atomContainer);
                }
            }
        }
    }
}
