﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class MoveScript : MonoBehaviour {

    public float speed = .7f;
    public float spacing = .1f;
    private Vector3 pos;

	// Use this for initialization
	void Start () {
		pos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.W))
            pos.y += spacing;
        if (Input.GetKeyDown(KeyCode.S))
            pos.y -= spacing;
        if (Input.GetKeyDown(KeyCode.A))
            pos.x -= spacing;
        if (Input.GetKeyDown(KeyCode.D))
            pos.x += spacing;

        if (Input.GetKeyDown(KeyCode.X))
            pos.z -= spacing;
        if (Input.GetKeyDown(KeyCode.Z))
            pos.z += spacing;


        transform.position = Vector3.MoveTowards(transform.position, pos, speed * Time.deltaTime);
    }
}
