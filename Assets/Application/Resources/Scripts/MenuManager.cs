﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class MenuManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadIntroductionScene()
    {
        SceneManager.LoadScene(1);
    }
    public void LoadLabScene()
    {
        SceneManager.LoadScene(2);
    }
    public void LoadTableScene()
    {
        SceneManager.LoadScene(3);
    }
}
