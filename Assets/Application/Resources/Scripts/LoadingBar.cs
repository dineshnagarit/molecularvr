﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class LoadingBar : MonoBehaviour
{

    #region Script's Summary
    //This script is displaying the LoadingBar
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------



    private Image self_Image_Component;

    private float fill_Time;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {

        //--------- variable definitions -----------

        self_Image_Component = GetComponentInChildren<Image>();

        fill_Time = 2f;
        self_Image_Component.fillAmount = 0;
       
      
        //-------------------------------------------
    }

    public void WaitForClick()
    {
        StartCoroutine(FillImage());
    }

    public IEnumerator FillImage()
    {
        while (true)
        {
            if (self_Image_Component.fillAmount != 1)
            {
                self_Image_Component.fillAmount += (Time.deltaTime / fill_Time);
            }
            else
            {
                self_Image_Component.fillAmount = 0;

                break;
            }
            yield return null;
        }

    }

    void disableLoading()
    {

        gameObject.SetActive(false);
    }


    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------



    //----------------------------------------------------------------------

    #endregion
}

