﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

/* This is a getter setter class 
 * used with bond energy json 
 */  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public partial class BondEnergy
{
    [JsonProperty("bondWith")]
    public List<BondWith> BondWith { get; set; }

    [JsonProperty("element")]
    public string Element { get; set; }
}
