﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class BondEnergyMap  {

    //private variables
    private Dictionary<BondEnergyKey, int> dictionaryEnergy=new Dictionary<BondEnergyKey, int>();

   /// <summary>
   /// 
   /// </summary>
   public struct BondEnergyKey
    {
        public string FirstAtom { get; set; }
        public string SecondAtom { get; set; }
        public int BondNum { get; set; }

        public bool Equals(BondEnergyKey obj)
        {
            return (FirstAtom == obj.FirstAtom && SecondAtom == obj.SecondAtom) || (FirstAtom == obj.SecondAtom && SecondAtom == obj.FirstAtom);
        }

        public override int GetHashCode()
        {
            ArrayList myAL = new ArrayList();
            myAL.Add(FirstAtom);
            myAL.Add(SecondAtom);
            myAL.Sort();

            unchecked
            {
                int hash = 17;
                hash = hash * 31 + myAL[0].GetHashCode();
                hash = hash * 31 + myAL[1].GetHashCode();
                return hash;
            }
        }
    }
    
    //adding data in map
     public void addDataToDictionary()
    {
        BondEnergyKey key1 = new BondEnergyKey();
        key1.FirstAtom = "H";
        key1.SecondAtom = "H";
        key1.BondNum = 1;
        dictionaryEnergy[key1] = 432;

        BondEnergyKey key2 = new BondEnergyKey();
        key2.FirstAtom = "H";
        key2.SecondAtom = "F";
        key2.BondNum = 1;
        dictionaryEnergy[key2] = 565;

        BondEnergyKey key3 = new BondEnergyKey();
        key3.FirstAtom = "H";
        key3.SecondAtom = "Cl";
        key3.BondNum = 1;
        dictionaryEnergy[key3] = 427;

        BondEnergyKey key4 = new BondEnergyKey();
        key4.FirstAtom = "H";
        key4.SecondAtom = "Br";
        key4.BondNum = 1;
        dictionaryEnergy[key4] = 363;

        BondEnergyKey key5 = new BondEnergyKey();
        key5.FirstAtom = "H";
        key5.SecondAtom = "I";
        key5.BondNum = 1;
        dictionaryEnergy[key5] = 295;

        BondEnergyKey key6 = new BondEnergyKey();
        key6.FirstAtom = "C";
        key6.SecondAtom = "H";
        key6.BondNum = 1;
        dictionaryEnergy[key6] = 413;

        BondEnergyKey key7 = new BondEnergyKey();
        key7.FirstAtom = "C";
        key7.SecondAtom = "C";
        key7.BondNum = 1;
        dictionaryEnergy[key7] = 347;

        BondEnergyKey key8 = new BondEnergyKey();
        key8.FirstAtom = "C";
        key8.SecondAtom = "C";
        key8.BondNum = 2;
        dictionaryEnergy[key8] = 614;

        BondEnergyKey key9 = new BondEnergyKey();
        key9.FirstAtom = "C";
        key9.SecondAtom = "C";
        key9.BondNum = 3;
        dictionaryEnergy[key9] = 839;

        BondEnergyKey key10 = new BondEnergyKey();
        key10.FirstAtom = "C";
        key10.SecondAtom = "N";
        key10.BondNum = 1;
        dictionaryEnergy[key10] = 305;

        BondEnergyKey key11 = new BondEnergyKey();
        key11.FirstAtom = "C";
        key11.SecondAtom = "O";
        key11.BondNum = 1;
        dictionaryEnergy[key11] = 358;

        BondEnergyKey key12 = new BondEnergyKey();
        key12.FirstAtom = "C";
        key12.SecondAtom = "F";
        key12.BondNum = 1;
        dictionaryEnergy[key12] = 485;

        BondEnergyKey key13 = new BondEnergyKey();
        key13.FirstAtom = "C";
        key13.SecondAtom = "Cl";
        key13.BondNum = 1;
        dictionaryEnergy[key13] = 339;

        BondEnergyKey key14 = new BondEnergyKey();
        key14.FirstAtom = "C";
        key14.SecondAtom = "Br";
        key14.BondNum = 1;
        dictionaryEnergy[key14] = 276;

        BondEnergyKey key15 = new BondEnergyKey();
        key15.FirstAtom = "C";
        key15.SecondAtom = "I";
        key15.BondNum = 1;
        dictionaryEnergy[key15] = 240;

        BondEnergyKey key16 = new BondEnergyKey();
        key16.FirstAtom = "C";
        key16.SecondAtom = "S";
        key16.BondNum = 1;
        dictionaryEnergy[key16] = 259;

        BondEnergyKey key17 = new BondEnergyKey();
        key17.FirstAtom = "C";
        key17.SecondAtom = "O";
        key17.BondNum = 2;
        dictionaryEnergy[key17] = 745;

        BondEnergyKey key18 = new BondEnergyKey();
        key18.FirstAtom = "C";
        key18.SecondAtom = "O";
        key18.BondNum = 3;
        dictionaryEnergy[key18] = 1072;

        BondEnergyKey key19 = new BondEnergyKey();
        key19.FirstAtom = "C";
        key19.SecondAtom = "N";
        key19.BondNum = 2;
        dictionaryEnergy[key19] = 615;

        BondEnergyKey key20 = new BondEnergyKey();
        key20.FirstAtom = "C";
        key20.SecondAtom = "N";
        key20.BondNum = 3;
        dictionaryEnergy[key20] = 891;

        BondEnergyKey key21 = new BondEnergyKey();
        key21.FirstAtom = "N";
        key21.SecondAtom = "H";
        key21.BondNum = 1;
        dictionaryEnergy[key21] = 391;

        BondEnergyKey key22 = new BondEnergyKey();
        key22.FirstAtom = "N";
        key22.SecondAtom = "N";
        key22.BondNum = 1;
        dictionaryEnergy[key22] = 160;

        BondEnergyKey key23 = new BondEnergyKey();
        key23.FirstAtom = "N";
        key23.SecondAtom = "F";
        key23.BondNum = 1;
        dictionaryEnergy[key23] = 272;

        BondEnergyKey key24 = new BondEnergyKey();
        key24.FirstAtom = "N";
        key24.SecondAtom = "Cl";
        key24.BondNum = 1;
        dictionaryEnergy[key24] = 200;

        BondEnergyKey key25 = new BondEnergyKey();
        key25.FirstAtom = "N";
        key25.SecondAtom = "Br";
        key25.BondNum = 1;
        dictionaryEnergy[key25] = 243;

        BondEnergyKey key26 = new BondEnergyKey();
        key26.FirstAtom = "N";
        key26.SecondAtom = "O";
        key26.BondNum = 1;
        dictionaryEnergy[key26] = 201;

        BondEnergyKey key27 = new BondEnergyKey();
        key27.FirstAtom = "N";
        key27.SecondAtom = "N";
        key27.BondNum = 2;
        dictionaryEnergy[key27] = 418;

        BondEnergyKey key28 = new BondEnergyKey();
        key28.FirstAtom = "N";
        key28.SecondAtom = "O";
        key28.BondNum = 2;
        dictionaryEnergy[key28] = 607;

        BondEnergyKey key29 = new BondEnergyKey();
        key29.FirstAtom = "N";
        key29.SecondAtom = "N";
        key29.BondNum = 3;
        dictionaryEnergy[key29] = 941;

        BondEnergyKey key30 = new BondEnergyKey();
        key30.FirstAtom = "O";
        key30.SecondAtom = "H";
        key30.BondNum = 1;
        dictionaryEnergy[key30] = 467;

        BondEnergyKey key31 = new BondEnergyKey();
        key31.FirstAtom = "O";
        key31.SecondAtom = "O";
        key31.BondNum = 1;
        dictionaryEnergy[key31] = 146;

        BondEnergyKey key32 = new BondEnergyKey();
        key32.FirstAtom = "O";
        key32.SecondAtom = "O";
        key32.BondNum = 2;
        dictionaryEnergy[key32] = 495;

        BondEnergyKey key33 = new BondEnergyKey();
        key33.FirstAtom = "O";
        key33.SecondAtom = "F";
        key33.BondNum = 1;
        dictionaryEnergy[key33] = 190;

        BondEnergyKey key34 = new BondEnergyKey();
        key34.FirstAtom = "O";
        key34.SecondAtom = "Cl";
        key34.BondNum = 1;
        dictionaryEnergy[key34] = 203;

        BondEnergyKey key35 = new BondEnergyKey();
        key35.FirstAtom = "O";
        key35.SecondAtom = "I";
        key35.BondNum = 1;
        dictionaryEnergy[key35] = 234;

        BondEnergyKey key36 = new BondEnergyKey();
        key36.FirstAtom = "F";
        key36.SecondAtom = "F";
        key36.BondNum = 1;
        dictionaryEnergy[key36] = 154;

        BondEnergyKey key37 = new BondEnergyKey();
        key37.FirstAtom = "F";
        key37.SecondAtom = "Cl";
        key37.BondNum = 1;
        dictionaryEnergy[key37] = 253;

        BondEnergyKey key38 = new BondEnergyKey();
        key38.FirstAtom = "F";
        key38.SecondAtom = "Br";
        key38.BondNum = 1;
        dictionaryEnergy[key38] = 237;

        BondEnergyKey key39 = new BondEnergyKey();
        key39.FirstAtom = "Na";
        key39.SecondAtom = "Cl";
        key39.BondNum = 1;
        dictionaryEnergy[key39] = 410;

        BondEnergyKey key40 = new BondEnergyKey();
        key40.FirstAtom = "Na";
        key40.SecondAtom = "F";
        key40.BondNum = 1;
        dictionaryEnergy[key40] = 481;

        BondEnergyKey key41 = new BondEnergyKey();
        key41.FirstAtom = "Na";
        key41.SecondAtom = "K";
        key41.BondNum = 1;
        dictionaryEnergy[key41] = 63;

        BondEnergyKey key42 = new BondEnergyKey();
        key42.FirstAtom = "Na";
        key42.SecondAtom = "I";
        key42.BondNum = 1;
        dictionaryEnergy[key41] = 301;

        BondEnergyKey key43 = new BondEnergyKey();
        key43.FirstAtom = "Li";
        key43.SecondAtom = "Cl";
        key43.BondNum = 1;
        dictionaryEnergy[key43] = 469;

        BondEnergyKey key44 = new BondEnergyKey();
        key44.FirstAtom = "Li";
        key44.SecondAtom = "Na";
        key44.BondNum = 1;
        dictionaryEnergy[key44] = 88;

        BondEnergyKey key45 = new BondEnergyKey();
        key45.FirstAtom = "Li";
        key45.SecondAtom = "F";
        key45.BondNum = 1;
        dictionaryEnergy[key45] = 577;

        BondEnergyKey key46 = new BondEnergyKey();
        key46.FirstAtom = "K";
        key46.SecondAtom = "Br";
        key46.BondNum = 1;
        dictionaryEnergy[key46] = 383;

        BondEnergyKey key47 = new BondEnergyKey();
        key47.FirstAtom = "K";
        key47.SecondAtom = "F";
        key47.BondNum = 1;
        dictionaryEnergy[key47] = 497;

        BondEnergyKey key48 = new BondEnergyKey();
        key48.FirstAtom = "K";
        key48.SecondAtom = "Na";
        key48.BondNum = 1;
        dictionaryEnergy[key48] = 63;
    }

     public Dictionary<BondEnergyKey,int> getDictionaryData()
    {
        return dictionaryEnergy;
    }
   
}
