﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class PanelManager : MonoBehaviour {

    public GameObject body;
    public TextAsset introText;
    public GameObject[] panels;
    private int count;
    public GameObject nextButton, prevButton;
    private Image playImage;
    private VideoPlayer player;
	// Use this for initialization
	void Start () {
        panels[0].SetActive(true);
        body.GetComponent<Text>().text = introText.text;
        count = 0;
        if (count == 0)
        {
            prevButton.SetActive(false);
        }
       
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    /// <summary>
    /// for loading next panel
    /// </summary>
    public void onNextClick()
    {
        if (panels.Length > 0 && count<panels.Length-1)
        {
            panels[count].SetActive(false);
            count++;
            panels[count].SetActive(true);
            prevButton.SetActive(true);

            playImage = panels[count].GetComponentInChildren<Image>();
            playImage.enabled = true;

            if (count == panels.Length - 1)
            {
                nextButton.SetActive(false);
            }
            player = panels[count].GetComponentInChildren<VideoPlayer>();
            if (player.isPlaying)
            {
                player.Stop();
            }
        }
        
    }
    /// <summary>
    /// Loading previous introduction panel
    /// </summary>
    public void onPreviousClick()
    {
        Debug.Log("prev count " + count);
        if (count>0)
        {
            player = panels[count].GetComponentInChildren<VideoPlayer>();
            panels[count].SetActive(false);
            count--;
            panels[count].SetActive(true);
            prevButton.SetActive(true);
            nextButton.SetActive(true);
            if (player.isPlaying)
            {
                player.Stop();
            }
            playImage = panels[count].GetComponentInChildren<Image>();
            if (playImage != null)
            {
                playImage.enabled = true;
            }

            if (count == 0)
            {
                prevButton.SetActive(false);
                nextButton.SetActive(true);
            }
        }
       

    }
    /// <summary>
    /// method for playing video on click
    /// </summary>
    public void PlayVideo()
    {
        Debug.Log("play video");
        
        player.Play();
        playImage.GetComponent<Image>().enabled = false;
      
    }
}
