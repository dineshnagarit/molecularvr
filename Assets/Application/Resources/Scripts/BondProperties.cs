﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class BondProperties {

    /// <summary>
    /// 
    /// </summary>
    public enum BondTypes { Ionic, NonPolarConvalent, PolarCovalent, Metallic, Hydrogen};

    public BondTypes bondtype;

    public int covalentBondType; // specifies whether covalent bond is single/double/triple

    public SpringJoint joint;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
