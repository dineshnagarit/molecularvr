﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;


/* This is a class which parses json
 * used with bond energy json 
 */  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class BondEnergies 
{
    public static BondEnergies FromJson(string json)
    {
        return JsonConvert.DeserializeObject<BondEnergies>(json, Settings);
    }
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
    };
    [JsonProperty("bondEnergies")]
    public List<BondEnergy> bondEnergyList { get; set; }
}
