﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class PalmButtonManager : MonoBehaviour {

    public GameObject AttachmentHands;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "MenuButton")
        {
            if (AttachmentHands.activeInHierarchy)
            {
                AttachmentHands.SetActive(false);
            }
            else
            {
                AttachmentHands.SetActive(true);
            }
        }
    }
}
