﻿using UnityEngine;
using System.Collections;
using Leap.Unity.Interaction;
using System;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

[RequireComponent(typeof(Rigidbody))]
public class InteractableItem : Leap.Unity.Interaction.InteractionBehaviour { // We may need to subclass this for InteractableAtom & InteractableMolecule if they have different behavior

	private Rigidbody rb;
	private bool currentlyInteracting;
	private Leap.Unity.Interaction.InteractionHand currentHandController;
    private Vector3 startPoint;
    private Quaternion startRotation;

	private float velocityFactor = 2000f;
	private float rotationFactor = .4f;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        rb = GetComponent<Rigidbody>();

        currentlyInteracting = false;
        velocityFactor /= rb.mass;
        rotationFactor /= rb.mass;

        if (rb == null)
        {
            Debug.Log("Interactable Item doesn't have a rigidbody");
        }

        // Add all important events
        OnGraspBegin += GraspBegin;
        OnGraspEnd += GraspEnd;
        OnGraspStay += GraspStay;
        OnPerControllerGraspBegin += PerControllerGraspBegin;
        OnPerControllerGraspEnd += PerControllerGraspEnd;

        OnContactBegin += ContactBegin;
        OnContactEnd += ContactEnd;
        OnContactStay += ContactStay;

    }

    private void PerControllerGraspEnd(InteractionController obj)
    {
        OnEndInteraction(obj);
    }

    private void PerControllerGraspBegin(InteractionController obj)
    {
        OnBeginInteraction(obj);
    }

    private void ContactStay()
    {
       // Debug.Log("Contact Stay");
    }

    private void ContactEnd()
    {
       // Debug.Log("Contact End");
    }

    private void ContactBegin()
    {
       // Debug.Log("Contact Begin");
    }

    private void GraspStay()
    {
      //  Debug.Log("GraspStay");
    }

    private void GraspEnd()
    {
      //  Debug.Log("GraspEnd");
    }

    private void GraspBegin()
    {
       // Debug.Log("GraspBegin");
    }

    // Update is called once per frame
    void FixedUpdate () {
		if (currentHandController && IsInteracting()) {

            //var posDelta = (new Vector3(currentHandController.leapHand.WristPosition.x, currentHandController.leapHand.WristPosition.y, currentHandController.leapHand.WristPosition.z)) - startPoint;
            //Debug.LogWarning("StartPoint = " + startPoint + " Current = " + new Vector3(currentHandController.leapHand.WristPosition.x, currentHandController.leapHand.WristPosition.y, currentHandController.leapHand.WristPosition.z));
            //var v = new Vector3(currentHandController.leapHand.PalmVelocity.x, currentHandController.leapHand.PalmVelocity.y, currentHandController.leapHand.PalmVelocity.z);
            //rb.velocity = v * velocityFactor * Time.fixedDeltaTime;

            //var rotationDelta = (new Quaternion(currentHandController.leapHand.Rotation.x, currentHandController.leapHand.Rotation.y, currentHandController.leapHand.Rotation.z, currentHandController.leapHand.Rotation.w)) * Quaternion.Inverse(startRotation);
            //float angle;
            //Vector3 axis;
            //rotationDelta.ToAngleAxis(out angle, out axis);

            //if (angle > 180)
            //{
            //    angle -= 360;
            //}

            //angle = Mathf.Deg2Rad * angle;

            //rb.angularVelocity = (Time.fixedDeltaTime * angle * axis) * rotationFactor;
        }
	}

	public void OnBeginInteraction(InteractionController hand) {
        currentHandController = hand.intHand;

		Debug.Log("enter interaction");
        startPoint = new Vector3(currentHandController.leapHand.WristPosition.x, currentHandController.leapHand.WristPosition.y, currentHandController.leapHand.WristPosition.z);
        startRotation = new Quaternion(currentHandController.leapHand.Rotation.x, currentHandController.leapHand.Rotation.y, currentHandController.leapHand.Rotation.z, currentHandController.leapHand.Rotation.w);

		currentlyInteracting = true;
	}

	public void OnEndInteraction(InteractionController hand) {
		if (hand.intHand == currentHandController) {
            currentHandController = null;
			currentlyInteracting = false;
		}
	}

	public bool IsInteracting() {
		return currentlyInteracting;
	}

}
