﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class ElectronRotation : MonoBehaviour {
    
    public float rotationSpeed;
    public Transform rotateAround;
    
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        //transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
        transform.RotateAround(rotateAround.position, Vector3.forward, rotationSpeed);
    }
    
}
