﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
/* This is a modal class for defining properties 
 * with which we are going to make bond
 */  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class BondWith
{
    [JsonProperty("bond_energy")]
    public string BondEnergy { get; set; }

    [JsonProperty("bond_type")]
    public string BondType { get; set; }

    [JsonProperty("element")]
    public string Element { get; set; }
}
