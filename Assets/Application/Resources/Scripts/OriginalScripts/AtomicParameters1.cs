﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class AtomicParameters1 : MonoBehaviour {

    public string atomName;
    public int atomicNumber;
    public float atomicMass;
    public float atomicRadius;
    public float electronegativity;
    public int sharedElectrons; // can be negative or positive
    // https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Periodic_table_%28polyatomic%29.svg/500px-Periodic_table_%28polyatomic%29.svg.png
    // https://en.wikipedia.org/wiki/Valence_electron
    public int groupNum; // group is periodic table column
    public int periodNum; // Period is periodic table row
    public bool hasPartialNegativeCharge; // by default atom isn't partial negative or positive
    public bool hasPartialPositiveCharge; // by default atom isn't partial negative or positive
    public string electronicConfiguration;
    int valenceElectrons = 0;

    public int electronsBorrowedOrGiven; // in case of ionic bond, atom borrow electrons or give away.// we will use +ve value if borrowed and negative if given

    public string uniqueId; // can be stored somewhere else but helps with debug

    public HashSet<BondProperties> bonds = new HashSet<BondProperties>();

    private int sharedSlots;

	// Use this for initialization
	void Start () {
        electronsBorrowedOrGiven = 0;
        hasPartialPositiveCharge = false;
        hasPartialNegativeCharge = false;
        sharedElectrons = 0;
        sharedSlots = 0;
    }

    public void Reset()
    {
        electronsBorrowedOrGiven = 0;
        hasPartialNegativeCharge = false;
        hasPartialPositiveCharge = false;
        sharedElectrons = 0;
        sharedSlots = 0;
    }

    // Update is called once per frame
    void Update () {

    }

    // this is different than partial charge
    public bool hasNegativeCharge()
    {
        if (electronsBorrowedOrGiven > 0)
            return true;
        return false;
    }

    public int getCharge()
    {
        return -1*electronsBorrowedOrGiven; // an atom which borrows electron gets negative charge
    }

    // this is different than partial charge
    public bool hasPositiveCharge()
    {
        if (electronsBorrowedOrGiven < 0)
            return true;
        return false;
    }

    public int getTotalElectrons()
    {
        return atomicNumber + sharedElectrons + electronsBorrowedOrGiven;
    }

    public void setActualValenceElectrons(int valElectrons)
    {
        valenceElectrons = valElectrons;
    }


    //https://www.wikihow.com/Find-Valence-Electrons
    public int getActualValenceElectrons()
    {
       
        // for now ignore group 3 to 12 since these behave differently (Transitional metals)
        if((groupNum >= 1 && groupNum < 3) || (groupNum >= 13 && groupNum <= 18))
        {
            if (groupNum < 10)
            {
                valenceElectrons = groupNum;
            }
            else
            {
                valenceElectrons = groupNum - 10;
            }
        } 
        else 
        {
            Debug.Log("getActualValenceElectrons not supported for this group elements");
        }
        return valenceElectrons;
    }

    // if atom is bonded with something, then it may have charge so add those shared electrons as well (it may have positive or negative value)
    public int getTotalValenceElectrons()
    {
        return getActualValenceElectrons() + getSharedElectrons() + electronsBorrowedOrGiven;
    }

    public int getSharedElectrons()
    {
        return sharedElectrons;
    }

    // returns electrons that can still be shared to form bonds
    public int getElectronsThatCanBeShared()
    {
        if (isStable())
        {
            return 0;
        }
        else
        {
            return getActualValenceElectrons() - getSharedElectrons();
        }
        
    }

    public bool isStable()
    {
        // an element can be stable if it doesn't have any empty slots i.e. either it have got electron from other atom or is sharing 
        // or it just gave away its valence electron hence making actual valence electron + electrons given to be 0
        return (getEmptySharableSlots() == 0 || (getActualValenceElectrons() + electronsBorrowedOrGiven == 0));
    }

    // this is empty slot that atom have to become stable & form bond
    public int getEmptySharableSlots()
    {
        return sharedSlots;
       // return getOrbitalPositions() - getTotalValenceElectrons();
    }

    // this gives how many electrons can be present in total in the last orbit
    public int getOrbitalPositions()
    {
        if (atomicNumber <= 18)
        {
            return 2 * periodNum * periodNum;
        }
        else
        {
            Debug.Log("Not supported for atomic number > 18");
            return 0;
        }

    }
    public void setEmptySharableSlots(int slots)
    {
        sharedSlots = slots;
    }
}
