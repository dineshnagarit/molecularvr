﻿using System.Collections;
using System.Collections.Generic;
using Leap.Unity.Interaction;
using UnityEngine;
using System;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(Rigidbody))]
public class AtomicInteractions1 : MonoBehaviour
{
    // for mapping atoms with bond num
    /// <summary>
    /// 
    /// </summary>
    public struct BondNumMap : IEquatable<BondNumMap>
    {
        public int FirstAtomId { get; set; }
        public int SecondAtomId { get; set; }

        public bool Equals(BondNumMap obj)
        {
            return (FirstAtomId == obj.FirstAtomId && SecondAtomId == obj.SecondAtomId) || (FirstAtomId == obj.SecondAtomId && SecondAtomId == obj.FirstAtomId);
        }

        public override int GetHashCode()
        {
            ArrayList myAL = new ArrayList();
            myAL.Add(FirstAtomId);
            myAL.Add(SecondAtomId);
            myAL.Sort();

            unchecked
            {
                int hash = 17;
                hash = hash * 31 + myAL[0].GetHashCode();
                hash = hash * 31 + myAL[1].GetHashCode();
                return hash;
            }
        }
    }

    private static Dictionary<BondNumMap, BondProperties> bondNumDict = new Dictionary<BondNumMap, BondProperties>();
    private Rigidbody rb;
    private HashSet<AtomicInteractions> atomsInEnergyField = new HashSet<AtomicInteractions>();
    private GameObject aura, aura1;
    private BondingRules bondingRules;

    // private static List<AtomicInteractions> atomsGrasped = new List<AtomicInteractions>();
    //private InteractableItem interactableItem;
    private bool areAtomGrasped;
    float currentForce = 0f;
    float breakforce = 0f;
    private GameObject molecule, collidedAtom;
    private static GameObject leftAtom, rightAtom;


    public SphereCollider energyFieldCollider; // this is collider that has bigger radius than actual sphere so it can be used to find whether anything is colliding in its energy field
    public SphereCollider actualCollider;
    [HideInInspector]
    public int[] nobleGases;
    //for getting distance between hands when atoms are grasped


    // Use this for initialization
    void Start()
    {
        bondingRules = gameObject.AddComponent<BondingRules>();
        bondingRules.getSetBondEnergyValues();
        rb = GetComponent<Rigidbody>();


        if (GetComponent<InteractableItem>())
        {
            var interactableItem = GetComponent<InteractableItem>();
            interactableItem.OnPerControllerGraspBegin += onAtomGrasped;
            interactableItem.OnPerControllerGraspEnd += onAtomReleased;
        }
        nobleGases = new int[] {2,8,18,32,56};
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("distance "+Vector3.Distance(leftPalm.transform.position,rightPalm.transform.position));
    }

    void FixedUpdate()
    {
        //Debug.Log("count " + atomsGrasped.Count);
        foreach (AtomicInteractions atom in atomsInEnergyField)
        {
            if (this.gameObject.transform.parent.gameObject.name.Contains("Molecule") && this.gameObject.transform.parent.gameObject == atom.gameObject.transform.parent.gameObject)
            {

            }
            else
            {
                // only add force if they are not part of same molecule else they are already bonded using spring bond
                // The idea here is that atoms/molecules/ion (remember atom don't have charge) would have attraction forces causing other atoms/ions to move closer and hence causing them to form bond
                Vector3 force = bondingRules.CalculateForce(gameObject.transform, atom);
                //Debug.Log("AtomID= " + atom.gameObject.GetComponent<AtomicParameters1>().uniqueId + " , force = " + force);
                rb.AddForce(force);
            }
        }

        //if (leftAtom != null && rightAtom != null)
        //{
        //    // both are part of same molecule
        //    if (leftAtom.transform.parent.gameObject.name.Contains("Molecule") && leftAtom.transform.parent.gameObject == rightAtom.transform.parent.gameObject)
        //    {
        //        BondNumMap bnp = new BondNumMap();
        //        bnp.FirstAtomId = leftAtom.transform.gameObject.GetInstanceID();
        //        bnp.SecondAtomId = rightAtom.transform.gameObject.GetInstanceID();

        //        Debug.Log("Query key FirstAtom InstID" + bnp.FirstAtomId + "  SecondAtom instId" + bnp.SecondAtomId);

        //        if (bondNumDict.ContainsKey(bnp))
        //        {
        //            var bp = bondNumDict[bnp];

        //            SpringJoint j = bp.joint;

        //            Debug.Log("Found key");
        //        }
        //        else
        //        {
        //            Debug.LogWarning("Key Not Found");
        //        }

        //        Rigidbody leftRB = leftAtom.gameObject.GetComponent<Rigidbody>();
        //        Rigidbody rightRB = rightAtom.gameObject.GetComponent<Rigidbody>();

        //        Vector3 dir = leftAtom.transform.position - rightAtom.transform.position;
        //        dir.Normalize();
        //        float dist = Vector3.Distance(leftAtom.transform.position, rightAtom.transform.position);
        //        Debug.Log("Distance between atoms " + dist + " dir " + dir);


        //        //leftRB.AddRelativeForce(dir * (dist* leftRB.mass), ForceMode.Acceleration);
        //       // rightRB.AddRelativeForce(-dir * (dist*rightRB.mass), ForceMode.Acceleration);
        //        Debug.LogWarning("Force Applied = " + dir * (500.0f * dist));
        //    }
        //}
    }



    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Atom") return;
        Debug.Log("entered collsion");
        ExecuteBondingRules(collision);
    }


    // https://www.khanacademy.org/science/biology/chemistry--of-life/chemical-bonds-and-reactions/v/electronegativity-and-chemical-bonds
    // https://socratic.org/questions/how-can-electronegativity-be-used-to-determine-the-type-of-bond-1
    // http://dl.clackamas.edu/ch104-07/electron.htm
    // https://www.khanacademy.org/science/biology/chemistry--of-life/chemical-bonds-and-reactions/v/ionic-covalent-and-metallic-bonds

    public void ExecuteBondingRules(Collision collisionInfo)
    {
        AtomicInteractions collidedAtom = collisionInfo.gameObject.GetComponent<AtomicInteractions>();
        var currentAtomParams = GetComponent<AtomicParameters1>();
        var collidedAtomParams = collidedAtom.GetComponent<AtomicParameters1>();
        float currentAtomElectroNegativity = currentAtomParams.electronegativity;
        float collidedAtomElectroNegativity = collidedAtomParams.electronegativity;

        // https://www.khanacademy.org/science/biology/chemistry--of-life/chemical-bonds-and-reactions/v/electronegativity-trends
        // Electronegativity is the ability of an atom to pull electrons towards itself.
        // use concept of electronegativity to find how badly wants to hog electrons
        if (currentAtomElectroNegativity > collidedAtomElectroNegativity)
        {
            // current wants to give electrons
            // now find how many electrons can this atom receive while how many electrons the other atom can give
            if (currentAtomParams.getEmptySharableSlots() > 0 && collidedAtomParams.getElectronsThatCanBeShared() > 0)
            {
               
                var minElectrons = Mathf.Min(currentAtomParams.getEmptySharableSlots(), collidedAtomParams.getElectronsThatCanBeShared());
                BondProperties.BondTypes bt = CalcBondType(currentAtomElectroNegativity, collidedAtomElectroNegativity);

                CreateBond(minElectrons, bt, collidedAtomParams, currentAtomElectroNegativity, collidedAtomElectroNegativity);
            }
        }
        else if (currentAtomElectroNegativity < collidedAtomElectroNegativity)
        {

            // now find how many electrons can this atom give while how many electrons the other atom can receive
            if (currentAtomParams.getElectronsThatCanBeShared() > 0 && collidedAtomParams.getEmptySharableSlots() > 0)
            {
                var minElectrons = Mathf.Min(currentAtomParams.getElectronsThatCanBeShared(), collidedAtomParams.getEmptySharableSlots());
                BondProperties.BondTypes bt = CalcBondType(currentAtomElectroNegativity, collidedAtomElectroNegativity);

                CreateBond(minElectrons, bt, collidedAtomParams, currentAtomElectroNegativity, collidedAtomElectroNegativity);
            }
        }
        else
        {
            if ((currentAtomParams.getEmptySharableSlots() > 0 && collidedAtomParams.getElectronsThatCanBeShared() > 0) || (currentAtomParams.getElectronsThatCanBeShared() > 0 && collidedAtomParams.getEmptySharableSlots() > 0))
            {
                // both have same electronegativity, in this case both 
                var minElectrons1 = Mathf.Min(currentAtomParams.getEmptySharableSlots(), collidedAtomParams.getElectronsThatCanBeShared());
                var minElectrons2 = Mathf.Min(currentAtomParams.getElectronsThatCanBeShared(), collidedAtomParams.getEmptySharableSlots());
                //Todo - should be take max of these mins or mins or these mins?IDK

                var minElectrons = Mathf.Max(minElectrons1, minElectrons2);
                if (minElectrons > 0)
                {
                    BondProperties.BondTypes bt = CalcBondType(currentAtomElectroNegativity, collidedAtomElectroNegativity);

                    CreateBond(minElectrons, bt, collidedAtomParams, currentAtomElectroNegativity, collidedAtomElectroNegativity);
                }
            }
        }
    }


    private bool IsHygrogenBondType(AtomicInteractions atom1, AtomicInteractions atom2)
    {
        // todo
        return false;
    }

    private BondProperties.BondTypes CalcBondType(float curAtomENeg, float collidedAtomENeg)
    {
        float absE = Mathf.Abs(curAtomENeg - collidedAtomENeg);
        if (absE < .5f)
        {
            return BondProperties.BondTypes.NonPolarConvalent;
        }
        else if (absE < 1.7f)
        {
            return BondProperties.BondTypes.PolarCovalent;
        }
        else
        {
            Debug.Log("Ionic Bond");
            return BondProperties.BondTypes.Ionic;
        }
    }

    private void CreateBond(int bondNum, BondProperties.BondTypes bondType, AtomicParameters1 collidedAtomParams, float curAtomENeg, float collidedAtomENeg)
    {
        if (bondNum < 1)
        {
            Debug.LogError("Bond can't be less than 1");
        }
        Debug.Log("Creating Bond");
        Debug.Log("Current Object uniqueId: " + GetComponent<AtomicParameters1>().uniqueId + " Collided Object uniqueId: " + collidedAtomParams.uniqueId);
        Debug.Log("Bond type: " + bondType.ToString() + " Bond Num: " + bondNum + " Bond between : " + this.gameObject.name + " & " + collidedAtomParams.gameObject.name);
        //bondNum means number of bonds between atoms i.e. single,double,triple 
        BondProperties bt = new BondProperties();
        bt.bondtype = bondType;
        bt.covalentBondType = bondNum;
        GetComponent<AtomicParameters1>().bonds.Add(bt);
        collidedAtomParams.bonds.Add(bt);
        BondNumMap bnp = new BondNumMap();
        bnp.FirstAtomId = gameObject.GetInstanceID();
        collidedAtom = collidedAtomParams.gameObject;
        bnp.SecondAtomId = collidedAtom.GetInstanceID();
        bondNumDict.Add(bnp, bt);
        Debug.Log("Adding Bond Key, FirstAtom InstID" + bnp.FirstAtomId + "  SecondAtom instId" + bnp.SecondAtomId);

        //replacing if else with switch cases for better readability

        switch (bondType)
        {
            case BondProperties.BondTypes.Ionic:
                if (curAtomENeg > collidedAtomENeg)
                {
                    // current atom gets negative charge while other atom gets positive charge i.e. it doesn't share electrons but borrows electrons from other atom
                    GetComponent<AtomicParameters1>().electronsBorrowedOrGiven = bondNum;
                    collidedAtomParams.electronsBorrowedOrGiven = -bondNum;
                    //after loosing and gaining electrons atom will acquire charges
                    GetComponent<AtomicParameters1>().hasPartialNegativeCharge = true;
                    GetComponent<AtomicParameters1>().hasPartialPositiveCharge = false;
                    collidedAtomParams.hasPartialPositiveCharge = true;
                    collidedAtomParams.hasPartialNegativeCharge = false;

                }
                else
                {
                    GetComponent<AtomicParameters1>().electronsBorrowedOrGiven = -bondNum;
                    collidedAtomParams.electronsBorrowedOrGiven = bondNum;
                    //after loosing and gaining electrons atom will acquire charges
                    GetComponent<AtomicParameters1>().hasPartialNegativeCharge = false;
                    GetComponent<AtomicParameters1>().hasPartialPositiveCharge = true;
                    collidedAtomParams.hasPartialPositiveCharge = false;
                    collidedAtomParams.hasPartialNegativeCharge = true;

                }
                break;
            case BondProperties.BondTypes.PolarCovalent:
            case BondProperties.BondTypes.NonPolarConvalent:
                // in these cases electrons are not given but only shared
                if (curAtomENeg > collidedAtomENeg)
                {
                    GetComponent<AtomicParameters1>().hasPartialPositiveCharge = false;
                    GetComponent<AtomicParameters1>().hasPartialNegativeCharge = true;
                    collidedAtomParams.hasPartialPositiveCharge = true;
                    collidedAtomParams.hasPartialNegativeCharge = false;
                }
                else if (curAtomENeg < collidedAtomENeg)
                {
                    GetComponent<AtomicParameters1>().hasPartialPositiveCharge = true;
                    GetComponent<AtomicParameters1>().hasPartialNegativeCharge = false;
                    collidedAtomParams.hasPartialPositiveCharge = false;
                    collidedAtomParams.hasPartialNegativeCharge = true;
                }

                collidedAtomParams.sharedElectrons += bondNum;
                GetComponent<AtomicParameters1>().sharedElectrons += bondNum;
                Debug.Log("Bond formed");
                Debug.Log("collided shared electron" + collidedAtomParams.sharedElectrons);
                Debug.Log("current shared electron" + GetComponent<AtomicParameters1>().sharedElectrons);
                break;
            default:
                break;
        }

      //  CreateBondVisualization(bondNum, collidedAtomParams, bt);
    }

    //private void CreateBondVisualization(int bondNum, AtomicParameters1 collidedAtomParams, BondProperties bt)
    //{
    //    //activating particle effect
    //    aura = collidedAtomParams.gameObject.transform.GetChild(1).gameObject;
    //    aura1 = gameObject.transform.GetChild(1).gameObject;


    //    //setting radius for particle system
    //    bondingRules.setParticleEffectRadius(aura1, gameObject);

    //    bondingRules.setParticleEffectRadius(aura, collidedAtomParams.gameObject);
    //    bondingRules.AddSpringJoint(gameObject, bondNum, collidedAtomParams, bt);

    //    //deactivating particle effect after 3 seconds
    //    Invoke("deActivateAura", 3f);

    //    //setting molecule prefab as parent for collided atoms
    //    bondingRules.createMolecule(gameObject, collidedAtomParams.gameObject);
    //}

  
    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag != "Atom") return;

        AtomicInteractions atom = collider.GetComponent<AtomicInteractions>();
        atomsInEnergyField.Add(atom);
        Debug.Log("trigger entered");
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag != "Atom") return;

        AtomicInteractions atom = collider.GetComponent<AtomicInteractions>();
        atomsInEnergyField.Remove(atom);
    }
    void deActivateAura()
    {
        // Debug.Log("name " + transform.GetChild(1).gameObject.GetInstanceID());
        aura.SetActive(false);
        aura1.SetActive(false);
    }

    private void onAtomGrasped(InteractionController ic)
    {
        if (ic.isLeft)
        {
            leftAtom = ic.graspedObject.gameObject;
            Debug.Log("isLeft" + leftAtom.transform.parent.GetInstanceID());
        }
        if (ic.isRight)
        {
            rightAtom = ic.graspedObject.gameObject;
            Debug.Log("is Right" + rightAtom.transform.parent.GetInstanceID());
        }

        if (leftAtom != null && rightAtom != null)
        {
            {
                //This means they belong to same parent molecule
                Debug.Log((leftAtom.transform.parent.GetInstanceID() == rightAtom.transform.parent.GetInstanceID()) && (leftAtom.GetInstanceID() != rightAtom.GetInstanceID()));
                if ((leftAtom.transform.parent.GetInstanceID() == rightAtom.transform.parent.GetInstanceID()) && (leftAtom.GetInstanceID() != rightAtom.GetInstanceID()))
                {
                    areAtomGrasped = true;
                }
                else
                {
                    return;
                }
            }
        }
    }


    private void onAtomReleased(InteractionController ic)
    {
        Debug.Log("atom released");
        areAtomGrasped = false;
        if (ic.isLeft)
        {
            leftAtom = null;
        }
        else{
            rightAtom = null;
        }
    }

    void breakJointBetween(GameObject atom1, GameObject atom2)
    {

    }


    void OnJointBreak(float breakforce)
    {
        // I think we can use the left and right atom used above as they will be the same atoms for which joint is broken as if they are not grasped there joint break will not call. I assume it is safe to use that
        aura = leftAtom.transform.GetChild(2).gameObject;
        aura1 = rightAtom.transform.GetChild(2).gameObject;
        aura.SetActive(true);
        aura1.SetActive(true);

        //deactivating particle effect after 2 seconds
        Invoke("deActivateAura", 2f);

        Debug.Log("joint broken " + breakforce);

        //this is buggy, the atom1 and atom2 here can be wrong, todo - keep a boolean when joint break is called and keep track of which solder joint is linked to which atom, then in update or fixedUpdate find which solderjoint is null now (since joint gets deleted when onJointBreak gets called)
        // remove from map in fixUpdate
        var atom1 = leftAtom;
        var atom2 = rightAtom;

        if (atom1 != null && atom2 != null)
        {
            Debug.Log("Left & right isn't null ");

            var molecule = atom1.transform.parent.gameObject;

            BondNumMap bnp = new BondNumMap();
            bnp.FirstAtomId = atom1.GetInstanceID();
            bnp.SecondAtomId = atom2.GetInstanceID();
            Debug.Log("OnJointBreak: Query key FirstAtom InstID" + bnp.FirstAtomId + "  SecondAtom instId" + bnp.SecondAtomId);

            if (!bondNumDict.ContainsKey(bnp))
            {
                Debug.LogError("OnJointBreak, key not found");
            }

            BondProperties bp = bondNumDict[bnp]; // must be found, if not found we have bug

            bool isatom1StillPartOfMolecule = true;
            bool isatom2StillPartOfMolecule = true;

            if (bp.bondtype == BondProperties.BondTypes.PolarCovalent || bp.bondtype == BondProperties.BondTypes.NonPolarConvalent)
            {
                atom1.GetComponent<AtomicParameters1>().sharedElectrons -= bp.covalentBondType;
                atom2.GetComponent<AtomicParameters1>().sharedElectrons -= bp.covalentBondType;

                if (atom1.GetComponent<AtomicParameters1>().sharedElectrons == 0)
                {
                    isatom1StillPartOfMolecule = false;
                }

                if (atom2.GetComponent<AtomicParameters1>().sharedElectrons == 0)
                {
                    isatom2StillPartOfMolecule = false;
                }
            }
            else if (bp.bondtype == BondProperties.BondTypes.Ionic)
            {
                var atomParameters1 = atom1.GetComponent<AtomicParameters1>();
                var atomParameters2 = atom2.GetComponent<AtomicParameters1>();


                Debug.Log("ionic bonds " + atomParameters1.electronsBorrowedOrGiven + " second atoms " + atomParameters2.electronsBorrowedOrGiven);
                atomParameters1.electronsBorrowedOrGiven= Mathf.Abs(atomParameters1.electronsBorrowedOrGiven) - bp.covalentBondType;
                atomParameters2.electronsBorrowedOrGiven = Mathf.Abs(atomParameters2.electronsBorrowedOrGiven) - bp.covalentBondType;

                Debug.Log("atom1 " + atomParameters1.electronsBorrowedOrGiven);
                Debug.Log("atom2 " + atomParameters2.electronsBorrowedOrGiven);

                if (atomParameters1.electronsBorrowedOrGiven == 0)
                {
                    isatom1StillPartOfMolecule = false;
                }
                if (atomParameters2.electronsBorrowedOrGiven == 0)
                {
                    isatom2StillPartOfMolecule = false;
                }






                //if (atom1.GetComponent<AtomicParameters1>().electronsBorrowedOrGiven + atom2.GetComponent<AtomicParameters1>().electronsBorrowedOrGiven != 0)
                //{
                //    // we may hit this condition - todo work todo, it could be possible that right atom got 1 atom from 1 ionic bond and other from other ionic bond
                //    // todo - vivek - fix it ?
                //    Debug.LogError("Ionic bond, electron borrowed or given are wrong");
                //}
                //else
                //{
                //    atom1.GetComponent<AtomicParameters1>().electronsBorrowedOrGiven = 0;
                //    atom2.GetComponent<AtomicParameters1>().electronsBorrowedOrGiven = 0;

                //    // need some logic - todo - vivek - pls fix it?
                //    //if (atom1.GetComponent<AtomicParameters1>().electronsBorrowedOrGiven == 0)
                //    //{
                //    //    isatom1StillPartOfMolecule = false;
                //    //}

                //    //if (atom2.GetComponent<AtomicParameters1>().electronsBorrowedOrGiven == 0)
                //    //{
                //    //    isatom2StillPartOfMolecule = false;
                //    //}
                //}
            }

            bondNumDict.Remove(bnp);

            var parentContainer = GameObject.FindGameObjectWithTag("Container");

            if (!isatom1StillPartOfMolecule)
            {
                atom1.transform.SetParent(null);
                atom1.transform.SetParent(parentContainer.transform);
            }

            if (!isatom2StillPartOfMolecule)
            {
                atom2.transform.SetParent(null);
                atom2.transform.SetParent(parentContainer.transform);
            }

            if (molecule.transform.childCount == 0)
            {
                Destroy(molecule);
            }

            // we may want to freeze positions of atom1 & atom2 temporarily so they won't go too far after joint is broken // todo (probably not, but we will see later)          

        }
        else
        {
            Debug.Log("Left or right or both are null");
        }
    }

    
}
