﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class ButtonManager : MonoBehaviour {

    public GameObject menuButtons;
    public Transform palm;
    [Range(0,1)]
    public float upValue = 0.5f;
    [Range(0, -1)]
    public float forwardValue = -0.7f;
    float currentupValue;
    float currentforwardValue;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        currentupValue = Vector3.Dot(palm.up, Vector3.up);
        currentforwardValue = Vector3.Dot(palm.right, Vector3.forward);
      //  Debug.Log("up value "+ currentupValue + " forward value "+ currentforwardValue);

        if (currentupValue > upValue && currentforwardValue < forwardValue)
        {
            //activate menu
             menuButtons.SetActive(true);
            Vector3 buttonPosition = new Vector3(transform.position.x, transform.position.y+0.09f,transform.position.z);
            menuButtons.transform.position = buttonPosition;
            //Debug.Log("show");

        }
        else
        {
            //deactivate menu
            menuButtons.SetActive(false);
            //Debug.Log("hide");
        }
    }
}
