﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class StickInteraction : MonoBehaviour
{

    #region Script's Summary
    //This script is for the functions that are performed when stick(or line renderer interacts with table or created atoms)
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public GameObject _atomPrefab;
    public GameObject _atomContainer;
   
    public Transform atomPos;
    public float spawnDistance = 0.05f;
    public Image progressImage;
    public float minPullDistance = 0.1f;
    public float pullSpeed = 1.15f;
    public bool canPull = false;
   

    //private variables
    private GameObject _currentCollidedElement;
    private Transform pulledAtom;
    private GameObject _currentTriggerElement;
    private float _coolDownTimer = 2f;
    private float _currentTime = 0f;
    private Vector3 _originalPosition;
    private AudioSource _audioPlayer;
    Collider previousCollider;
    private Transform _originalTransform;
    private GameObject palmButton;
    private float offset = 0.0f;
    private float speedOffset = 0.0f;
    private float fill_Time;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {
        _audioPlayer = GetComponent<AudioSource>();
      //  Debug.Log("IndexFingerCreated");

        fill_Time = 2f;
        progressImage.fillAmount = 0;
        //-------------------------------------------
    }


    void Update()
    {
        if (_currentTime > 0)
            _currentTime -= Time.deltaTime;

        if (canPull)
        {
           // Debug.Log("distance " + Vector3.Distance(pulledAtom.position, transform.position));
            if (pulledAtom != null && (Vector3.Distance(pulledAtom.position, transform.position) > minPullDistance))
            {
                pulledAtom.position = Vector3.Lerp(pulledAtom.position, transform.position, pullSpeed * Time.deltaTime);
                //gameObject.SetActive(false);
            }
            else
            {
                canPull = false;
                pulledAtom = null;
            }
        }
    }

    void OnCollisionExit(Collision collisionInfo)
    {
        Debug.Log("OnCollisionExit");
    }

    void OnCollisionEnter(Collision _collision)
    {
        Debug.Log("OnCollisionEnter");
    }

    void OnCollisionStay(Collision _collision)
    {
        Debug.Log("OnCollisionStay");
    }



    void OnTriggerStay(Collider other)
    {

      
        if (gameObject.tag == "Stick" && other.gameObject.tag == "TableElement")//this means stick is being used for creating atom
        {
            progressImage.gameObject.transform.parent.gameObject.SetActive(true);
            Debug.Log("stay");
            loader(other);
        }

    }

    void OnTriggerExit(Collider other)
    {
        progressImage.gameObject.transform.parent.gameObject.SetActive(false);

    }



    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Molecule" || other.gameObject.tag == "Atom")
        {
            pulledAtom = other.gameObject.transform;
            canPull = true;
        }
    }

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------
    void ElementReleasedFromContact()
    {
        if (!_currentTriggerElement)
            return;

        if (_currentTriggerElement.tag == "TableElement")
        { // restore position if button hit was atom only
            _currentTriggerElement.transform.position = _originalPosition;
            Debug.Log("restored position" + _currentTriggerElement.transform.position);
            // Debug.Log("released " + _currentTriggerElement.name);
            _currentTriggerElement = null;
        }
    }

    public void createAtom(GameObject triggerObject, GameObject atomPrefab, GameObject atomContainer, bool isInteractableItem = true)
    {
        Vector3 spawnPosition = atomPos.transform.position;
        Debug.Log("pos " + spawnPosition);
        GameObject atom = Instantiate(atomPrefab, spawnPosition, Quaternion.identity, atomContainer.transform);
        GameObject atomWithForceField = atom.transform.GetChild(1).gameObject;


        // getting atom radius and scale it down
        float radius = (triggerObject.GetComponent<Element>().atomicRadius) / 1500;

        // Getting color from the collided element and assigning to atom
        MeshRenderer[] r = triggerObject.GetComponentsInChildren<MeshRenderer>();
        Material mat = r[0].materials[0];
        Color color = mat.color;
        atom.GetComponent<Renderer>().material.color = color;

        string atomName = triggerObject.GetComponent<Element>().ElementName.text;
        TextMesh textMesh = atom.GetComponentInChildren<TextMesh>();
        textMesh.text = atomName;
        atom.name = atomName;

        var atomicElement = triggerObject.GetComponent<Element>();

        atom.GetComponent<Rigidbody>().mass = atomicElement.DataAtomicWeight;
        var atomicParams = atom.GetComponent<AtomicParameters>();
        atomicParams.atomicMass = atomicElement.DataAtomicWeight;
        atomicParams.atomicNumber = int.Parse(atomicElement.DataAtomicNumber.text);
        atomicParams.atomName = atomicElement.ElementName.text;
        atomicParams.atomicRadius = radius;
        atomicParams.electronegativity = atomicElement.electronegativity;
        atomicParams.groupNum = atomicElement.group;
        atomicParams.periodNum = atomicElement.period;
        atomicParams.uniqueId = System.Guid.NewGuid().ToString();

        //setting radius 
        atom.transform.localScale = new Vector3(radius, radius, radius);

        if (!isInteractableItem && atom.GetComponent<InteractableItem>())
        {
            Destroy(atom.GetComponent<InteractableItem>());
        }
    }

    public void loader(Collider other)
    {
        float fill_Time = 2f;
        if (progressImage.fillAmount != 1)
        {
            // Debug.Log(loadingImage.fillAmount);
            progressImage.fillAmount += (Time.deltaTime / fill_Time);
        }
        else
        {
            onAtomClick(other);
            progressImage.gameObject.transform.parent.gameObject.SetActive(false);
            //Debug.Log("OnTriggerExit");
            progressImage.fillAmount = 0;
            Invoke("ElementReleasedFromContact", .5f);
        }
    }
    //----------------------------------------------------------------------
    public void onAtomClick(Collider other)
    {
        _currentTriggerElement = other.gameObject;
        // Debug.Log("entered " + _currentTriggerElement.name);
        _originalPosition = _currentTriggerElement.transform.position;
        Debug.Log("original position" + _originalPosition);
        _audioPlayer.Play();

        _currentTime = _coolDownTimer;

        Transform element = _currentTriggerElement.transform;
        element.position = new Vector3(element.position.x, element.position.y, element.position.z + 0.05f);
        Debug.Log("clicked position" + element.position);

        
        createAtom(_currentTriggerElement, _atomPrefab, _atomContainer);
    }
    #endregion
}