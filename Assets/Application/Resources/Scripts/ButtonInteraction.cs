﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class ButtonInteraction : MonoBehaviour
{

    #region Script's Summary
    //This script is 
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public GameObject _periodicTable;
    public GameObject _periodicButtons;
    public Texture[] _switchImages;
    public GameObject stick;


    private float yPos;
    private float _tableYPosition = 0.1f;
    private float _maxTableDistance = 0.5f;
    private float _minTableDistance = 0.3f;
    private float _maxTableUpLimit = 0.5f;
    private float _maxTableDownLimit = -0.25f;
    private float _maxBackLimit = 0.15f;
    private float _maxForwardLimit = -0.1f;
    private float coolDownTimer = 1f;
    private float currentTime = 0f;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {
        //--------- variable definitions -----------



        //-------------------------------------------
    }

    /*
	void Update () {
		
	}
	*/

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    

    public void HoldInteraction(GameObject _button, Vector3 _newPosition)
    {
        switch (_button.name)
        {
            case "Move":
                {
                    MovePeriodicTable(_newPosition);
                   // Notification._message = "Move the Table";
                }
                break;
            case "Switch":
                {
                  //  Notification._message = "Show/Hide the table";
                }
                break;
        }
    }

    /// <summary>
    /// enable or disable the periodic table
    /// </summary>
  public void SwitchButton(GameObject _button)
    {
        if (_periodicTable.activeInHierarchy)
        {
            _periodicButtons.SetActive(false);
            _periodicTable.SetActive(false);
            _button.GetComponent<Renderer>().material.mainTexture = _switchImages[1];
        }
        else
        {
            _periodicButtons.SetActive(true);
            _periodicTable.SetActive(true);
            _button.GetComponent<Renderer>().material.mainTexture = _switchImages[0];
        }
    }

    public void LaserToggleButton()
    {
        if (stick.activeInHierarchy)
        {
            currentTime = coolDownTimer;
            stick.SetActive(false);
        }
        else
        {
            currentTime = coolDownTimer;
            stick.SetActive(true);
        }
    }
    /// <summary>
    /// move periodic table up
    /// </summary>

    const float MOVEBY = 0.005f;

    public void UpButton()
    {

        if (_periodicTable.transform.position.y < _maxTableUpLimit)
        {
            _periodicTable.transform.position = new Vector3(_periodicTable.transform.position.x, _periodicTable.transform.position.y + MOVEBY, _periodicTable.transform.position.z);
        }
    }
    /// <summary>
    /// move periodic table down
    /// </summary>
    public void DownButton()
    {

        if (_periodicTable.transform.position.y > _maxTableDownLimit)
        {
            _periodicTable.transform.position = new Vector3(_periodicTable.transform.position.x, _periodicTable.transform.position.y - MOVEBY, _periodicTable.transform.position.z);
        }
    }

    /// <summary>
    /// move periodic table back
    /// </summary>
    public void BackwardButton()
    {
      
        //if (_periodicTable.transform.localPosition.z < _maxBackLimit)
        {
            _periodicTable.transform.localPosition = new Vector3(_periodicTable.transform.localPosition.x, _periodicTable.transform.localPosition.y, _periodicTable.transform.localPosition.z + MOVEBY);
        }
    }

    /// <summary>
    /// move periodic table forward
    /// </summary>
    public void ForwardButton()
    {
        //if (_periodicTable.transform.localPosition.z > _maxForwardLimit)
        {
            _periodicTable.transform.localPosition = new Vector3(_periodicTable.transform.localPosition.x, _periodicTable.transform.localPosition.y, _periodicTable.transform.localPosition.z - MOVEBY);
        }
    }

    public void UpDownSlide(Leap.Unity.Interaction.InteractionSlider slider)
    {
        float f;
        f = slider.normalizedHorizontalValue * (_maxTableUpLimit - _maxTableDownLimit) + _maxTableDownLimit;
        _periodicTable.transform.position = new Vector3(_periodicTable.transform.position.x, f, _periodicTable.transform.position.z);

        Debug.Log("slided"+ f);
    }
    public void BackandForthSlide(Leap.Unity.Interaction.InteractionSlider slider)
    {
        float f;
        f = slider.normalizedHorizontalValue * (_maxForwardLimit - _maxBackLimit) + _maxBackLimit;
        _periodicTable.transform.position = new Vector3(_periodicTable.transform.position.x, _periodicTable.transform.position.y,f);

        Debug.Log("back forth" + f);
    }
    /// <summary>
    /// change the position of the periodic table
    /// </summary>
    void MovePeriodicTable(Vector3 _newPosition)
    {

        transform.parent.position = new Vector3(0, _newPosition.y, _newPosition.z);

        if (_newPosition.z > _minTableDistance)
            transform.parent.position = new Vector3(0, _tableYPosition, _minTableDistance);
        if (_newPosition.z < _maxTableDistance)
            transform.parent.position = new Vector3(0, _tableYPosition, _maxTableDistance);
    }

    //----------------------------------------------------------------------

    #endregion
}
