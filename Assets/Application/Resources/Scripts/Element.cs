﻿//
// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class Element : MonoBehaviour
{
    public static Element ActiveElement;

    public TextMesh ElementNumber;
    public TextMesh ElementName;
    public TextMesh ElementNameDetail;

    public Text ElementDescription;
    public Text DataAtomicNumber;
    public float DataAtomicWeight;
    public Text DataMeltingPoint;
    public Text DataBoilingPoint;
    public float electronegativity;

    public Renderer BoxRenderer;
    public MeshRenderer[] PanelSides;
    public MeshRenderer PanelFront;
    public MeshRenderer PanelBack;
    public MeshRenderer[] InfoPanels;
    public float atomicRadius;
    public int group;
    public int period;
    public string electronicConfiguration;

    public NewAtom Atom;

    [HideInInspector]
    public ElementData data;

    private BoxCollider boxCollider;
    private Material highlightMaterial;
    private Material dimMaterial;
    private Material clearMaterial;
   

    public void Start()
    {
        BoxRenderer.enabled = true;
        
    }


    public void Highlight()
    {
        if (ActiveElement == this)
            return;

        for (int i = 0; i < PanelSides.Length; i++)
        {
            PanelSides[i].sharedMaterial = highlightMaterial;
        }
        PanelBack.sharedMaterial = highlightMaterial;
        PanelFront.sharedMaterial = highlightMaterial;
        BoxRenderer.sharedMaterial = highlightMaterial;
    }

    public void Dim()
    {
        if (ActiveElement == this)
            return;

        for (int i = 0; i < PanelSides.Length; i++)
        {
            PanelSides[i].sharedMaterial = dimMaterial;
        }
        PanelBack.sharedMaterial = dimMaterial;
        PanelFront.sharedMaterial = dimMaterial;
        BoxRenderer.sharedMaterial = dimMaterial;
    }

    


    /**
     * Set the display data for this element based on the given parsed JSON data
     */
    public void SetFromElementData(ElementData data, Dictionary<string, Material> typeMaterials)
    {
        this.data = data;
        ElementNumber.text = data.atomicNumber;
        ElementName.text = data.symbol;
        ElementNameDetail.text = data.name;
        atomicRadius = data.vanDelWaalsRadius/2.0f;
        //ElementDescription.text = data.summary;
        DataAtomicNumber.text = data.atomicNumber;
        DataAtomicWeight = data.atomicMass;
        electronegativity = data.electronegativity;
        group = data.xpos;
        period = data.ypos;
        electronicConfiguration = data.electronicConfiguration;
       // DataMeltingPoint.text = data.melt.ToString();
       // DataBoilingPoint.text = data.boil.ToString();

        Color newCol;

        string c = data.cpkHexColor;
        string htmlc = "#" + c;

        ColorUtility.TryParseHtmlString(htmlc, out newCol);

        // Set up our materials
        if (!typeMaterials.TryGetValue(data.groupBlock.Trim(), out dimMaterial))
        {
            Debug.Log("Couldn't find " + data.groupBlock.Trim() + " in element " + data.name);
        }
        //dimMaterial.color = newCol; // changing this will change actual material file, don't uncomment.
        // Create a new highlight material and add it to the dictionary so other can use it
        string highlightKey = data.groupBlock.Trim() + " highlight";
        if (!typeMaterials.TryGetValue(highlightKey, out highlightMaterial))
        {
            highlightMaterial = new Material(dimMaterial);
            highlightMaterial.color = highlightMaterial.color * 1.5f;
            typeMaterials.Add(highlightKey, highlightMaterial);
        }

        Dim();

        Atom.NumElectrons = int.Parse(data.atomicNumber);
        Atom.NumNeutrons = (int)data.atomicMass / 2;
        Atom.NumProtons = (int)data.atomicMass / 2;
        Atom.Radius = data.vanDelWaalsRadius/2.0f;//TEMP

        foreach (Renderer infoPanel in InfoPanels)
        {
            // Copy the color of the element over to the info panels so they match
            infoPanel.material.color = dimMaterial.color;
        }

        BoxRenderer.enabled = false;

        // Set our name so the container can alphabetize
        transform.parent.name = data.name;
    }
}
