﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class TableInFront : MonoBehaviour {

    public GameObject cam;

    private int currentFrame = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        currentFrame++;

        if (currentFrame == 3)
        {
            this.transform.position = cam.transform.position;
			StartCoroutine(scaleTable());
		}
	}
	public IEnumerator scaleTable()
	{

		while (transform.localScale.y < 1)
		{
			Debug.Log("count");
			transform.localScale += new Vector3(0,1*Time.deltaTime * 0.3f,0);
			yield return null;
		}
	}
}
