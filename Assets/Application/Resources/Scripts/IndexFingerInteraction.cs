﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class IndexFingerInteraction : MonoBehaviour
{

    #region Script's Summary
    //This script is for the functions that are performed when index finger of  hand interacts with different objects
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public GameObject _atomPrefab;
    public GameObject _atomContainer;
    public Transform atomPos;
    public float spawnDistance = 0.05f;
    public Image loadingImage;

    //private variables
    private GameObject _currentCollidedElement;
    private GameObject _currentTriggerElement;
    private float _coolDownTimer = 2f;
    private float _currentTime = 0f;
    private Vector3 _originalPosition;
    private AudioSource _audioPlayer;
    Collider previousCollider;
    private Transform _originalTransform;
    private GameObject palmButton;
    private float offset = 0.0f;
    private float speedOffset = 0.0f;
    private float fill_Time;
    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {
        _audioPlayer = GetComponent<AudioSource>();
        //Debug.Log("IndexFingerCreated");

        fill_Time = 2f;
        loadingImage.fillAmount = 0;
        //-------------------------------------------
    }


    void Update()
    {
        if (_currentTime > 0)
            _currentTime -= Time.deltaTime;


    }

    void OnCollisionExit(Collision collisionInfo)
    {
      //  Debug.Log("OnCollisionExit");
    }

    void OnCollisionEnter(Collision _collision)
    {
      //  Debug.Log("OnCollisionEnter");
    }

    void OnCollisionStay(Collision _collision)
    {
       // Debug.Log("OnCollisionStay");
    }



    void OnTriggerStay(Collider other)
    {
        //loadingImage.gameObject.transform.parent.gameObject.SetActive(true);
        //if (gameObject.tag=="Stick" && other.gameObject.tag=="TableElement")//this means stick is being used for creating atom
        //{
        //   // loader(other);

        //}

    }

    void OnTriggerExit(Collider other)
    {
        //loadingImage.gameObject.transform.parent.gameObject.SetActive(false);
        ////Debug.Log("OnTriggerExit");
        //loadingImage.fillAmount = 0;
    }



    void OnTriggerEnter(Collider other)
    {
        // Debug.Log(gameObject.name +" coll " + other.name);
        if (_currentTime > 0)
            return;
        //when finger is used for creating atom
        if (other.gameObject.tag.Equals("TableElement"))
        {
           // Debug.Log("atom from trigger enter" + "Tag " + gameObject.tag);
            onAtomClick(other);
        }

    }

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------
    void ElementReleasedFromContact()
    {
        if (!_currentTriggerElement)
            return;

        if (_currentTriggerElement.tag == "TableElement") // restore position if button hit was atom only
            _currentTriggerElement.transform.position = _originalPosition;
     //   Debug.Log("restored position" + _currentTriggerElement.transform.position);
        // Debug.Log("released " + _currentTriggerElement.name);
        _currentTriggerElement = null;
    }

    public void createAtom(GameObject triggerObject, GameObject atomPrefab, GameObject atomContainer, bool isInteractableItem = true)
    {
        Vector3 spawnPosition = atomPos.transform.position;
      //  Debug.Log("pos " + spawnPosition);
        GameObject atom = Instantiate(atomPrefab, spawnPosition, Quaternion.identity, atomContainer.transform);
        GameObject atomWithForceField = atom.transform.GetChild(1).gameObject;


        // getting atom radius and scale it down
        float radius = (triggerObject.GetComponent<Element>().atomicRadius) / 1500;

        // Getting color from the collided element and assigning to atom
        MeshRenderer[] r = triggerObject.GetComponentsInChildren<MeshRenderer>();
        Material mat = r[0].materials[0];
        Color color = mat.color;
        atom.GetComponent<Renderer>().material.color = color;

        string atomName = triggerObject.GetComponent<Element>().ElementName.text;
        TextMesh textMesh = atom.GetComponentInChildren<TextMesh>();
        textMesh.text = atomName;
        atom.name = atomName;

        var atomicElement = triggerObject.GetComponent<Element>();

        atom.GetComponent<Rigidbody>().mass = atomicElement.DataAtomicWeight;
      //  var atomicParams = atom.GetComponent<AtomicParameters>();
        var atomicParams = atom.GetComponent<AtomicParameters>();

        atomicParams.atomicMass = atomicElement.DataAtomicWeight;
        atomicParams.atomicNumber = int.Parse(atomicElement.DataAtomicNumber.text);
        atomicParams.atomName = atomicElement.ElementName.text;
        atomicParams.atomicRadius = radius;
        atomicParams.electronegativity = atomicElement.electronegativity;
        atomicParams.groupNum = atomicElement.group;
        atomicParams.periodNum = atomicElement.period;
        atomicParams.uniqueId = System.Guid.NewGuid().ToString();
        atomicParams.electronicConfiguration = atomicElement.electronicConfiguration;

        //setting radius 
        atom.transform.localScale = new Vector3(radius, radius, radius);

        if (!isInteractableItem && atom.GetComponent<InteractableItem>())
        {
            Destroy(atom.GetComponent<InteractableItem>());
        }
    }

    //public void loader(Collider other)
    //{
    //    float fill_Time = 1f;
    //    if (loadingImage.fillAmount != 1)
    //    {
    //        Debug.Log(loadingImage.fillAmount);
    //        loadingImage.fillAmount += (Time.deltaTime / fill_Time);
    //    }
    //    else
    //    {
    //        onAtomClick(other);
    //    }
    //}
    //----------------------------------------------------------------------
    public void onAtomClick(Collider other)
    {
        _currentTriggerElement = other.gameObject;
        // Debug.Log("entered " + _currentTriggerElement.name);
        _originalPosition = _currentTriggerElement.transform.position;
      //  Debug.Log("original position" + _originalPosition);
        _audioPlayer.Play();

        _currentTime = _coolDownTimer;

        Transform element = _currentTriggerElement.transform;
        element.position = new Vector3(element.position.x, element.position.y, element.position.z + 0.05f);
      //  Debug.Log("clicked position" + element.position);

        Invoke("ElementReleasedFromContact", .5f);
        createAtom(_currentTriggerElement, _atomPrefab, _atomContainer);
    }
    #endregion
}