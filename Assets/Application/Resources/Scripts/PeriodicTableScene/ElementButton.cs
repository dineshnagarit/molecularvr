﻿//
// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.
//


using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TableScene
{
    /// <summary>
    /// 
    /// </summary>
    /// <code>
    /// 
    /// </code>
    public class ElementButton : Button
    {

        

        public  void Pressed()
        {
            
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "IndexFinger")
            {
                Element element = gameObject.GetComponent<Element>();
                Debug.Log("element clicked");
                // User has clicked us
                // If we're the active element button, reset ourselves
                if (Element.ActiveElement == element)
                {
                    // If we're the current element, reset ourselves
                    Element.ActiveElement = null;
                }
                else
                {
                    Element.ActiveElement = element;
                    element.Open();
                }
            }
        }
        //private void OnMouseDown()
        //{
        //    Element element = gameObject.GetComponent<Element>();
        //    Debug.Log("element clicked");
        //    // User has clicked us
        //    // If we're the active element button, reset ourselves
        //    if (Element.ActiveElement == element)
        //    {
        //        // If we're the current element, reset ourselves
        //        Element.ActiveElement = null;
        //    }
        //    else
        //    {
        //        Element.ActiveElement = element;
        //        element.Open();
        //    }
        //}

        public override string ToString()
        {
            return base.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object other)
        {
            return base.Equals(other);
        }

        protected override void Start()
        {
            base.Start();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }

        public override bool IsActive()
        {
            return base.IsActive();
        }

        protected override void OnRectTransformDimensionsChange()
        {
            base.OnRectTransformDimensionsChange();
        }

        protected override void OnBeforeTransformParentChanged()
        {
            base.OnBeforeTransformParentChanged();
        }

        protected override void OnTransformParentChanged()
        {
            base.OnTransformParentChanged();
        }

        protected override void OnCanvasHierarchyChanged()
        {
            base.OnCanvasHierarchyChanged();
        }

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void OnCanvasGroupChanged()
        {
            base.OnCanvasGroupChanged();
        }

        public override bool IsInteractable()
        {
            return base.IsInteractable();
        }

        protected override void OnDidApplyAnimationProperties()
        {
            base.OnDidApplyAnimationProperties();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        //protected override void OnValidate()
        //{
        //    base.OnValidate();
        //}

        //protected override void Reset()
        //{
        //    base.Reset();
        //}

        protected override void InstantClearState()
        {
            base.InstantClearState();
        }

        protected override void DoStateTransition(SelectionState state, bool instant)
        {
            base.DoStateTransition(state, instant);
        }

        public override Selectable FindSelectableOnLeft()
        {
            return base.FindSelectableOnLeft();
        }

        public override Selectable FindSelectableOnRight()
        {
            return base.FindSelectableOnRight();
        }

        public override Selectable FindSelectableOnUp()
        {
            return base.FindSelectableOnUp();
        }

        public override Selectable FindSelectableOnDown()
        {
            return base.FindSelectableOnDown();
        }

        public override void OnMove(AxisEventData eventData)
        {
            base.OnMove(eventData);
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);
        }

        public override void OnDeselect(BaseEventData eventData)
        {
            base.OnDeselect(eventData);
        }

        public override void Select()
        {
            base.Select();
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
        }

        public override void OnSubmit(BaseEventData eventData)
        {
            base.OnSubmit(eventData);
        }
    }
}
