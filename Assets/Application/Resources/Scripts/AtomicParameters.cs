﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class AtomicParameters : MonoBehaviour {

    public string atomName;
    public int atomicNumber;
    public float atomicMass;
    public float atomicRadius;
    public float electronegativity;
	
    // can be negative or positive
    // https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Periodic_table_%28polyatomic%29.svg/500px-Periodic_table_%28polyatomic%29.svg.png
    // https://en.wikipedia.org/wiki/Valence_electron
    public int groupNum; // group is periodic table column
    public int periodNum; // Period is periodic table row
    public bool hasPartialNegativeCharge; // by default atom isn't partial negative or positive
    public bool hasPartialPositiveCharge; // by default atom isn't partial negative or positive
    public string electronicConfiguration;
    int valenceElectrons = 0;

    public int electronsBorrowedOrGiven; // in case of ionic bond, atom borrow electrons or give away.// we will use +ve value if borrowed and negative if given

    public string uniqueId; // can be stored somewhere else but helps with debug

    public HashSet<BondProperties> bonds = new HashSet<BondProperties>();

    public int emptySlots=0;
    public int sharedElectron = 0;
    private int electronThatCanBeShared = 0;

    // Use this for initialization
    void Start () {
        electronsBorrowedOrGiven = 0;
        hasPartialPositiveCharge = false;
        hasPartialNegativeCharge = false;
    }

    /// <summary>
    /// reset everything to default values
    /// </summary>
    public void Reset()
    {
        electronsBorrowedOrGiven = 0;
        hasPartialNegativeCharge = false;
        hasPartialPositiveCharge = false;
        sharedElectron = 0;
        emptySlots = 0;
        electronThatCanBeShared = 0;
    }

    // Update is called once per frame
    void Update () {

    }

    // this is different than partial charge
    public bool hasNegativeCharge()
    {
        if (electronsBorrowedOrGiven > 0)
            return true;
        return false;
    }

    public int getCharge()
    {
        return -1*electronsBorrowedOrGiven; // an atom which borrows electron gets negative charge
    }

    // this is different than partial charge
    public bool hasPositiveCharge()
    {
        if (electronsBorrowedOrGiven < 0)
            return true;
        return false;
    }


    public void setElectronThatCanBeShared(int sharedElectrons)
    {
        electronThatCanBeShared = sharedElectrons;
    }

    // returns electrons that can still be shared to form bonds
    public int getElectronsThatCanBeShared()
    {
        if (isStable())
        {
            return 0;
        }
        else
        {
            return electronThatCanBeShared;
        }
        
    }
    

    public bool isStable()
    {
        // an element can be stable if it doesn't have any empty slots i.e. either it have got electron from other atom or is sharing 
        // or it just gave away its valence electron hence making actual valence electron + electrons given to be 0
        return false;
    }

    // this is empty slot that atom have to become stable & form bond
    public int getEmptySharableSlots()
    {
        return emptySlots;
    }

    public void setEmptySharableSlots(int slots)
    {
        emptySlots = slots;
    }
}
