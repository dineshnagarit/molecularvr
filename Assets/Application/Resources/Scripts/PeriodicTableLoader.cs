﻿//
// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.
//

using System.Collections.Generic;
using System.Collections;
using UnityEngine;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

[System.Serializable]
public class ElementData
{
    public string name;
    public string groupBlock;
    public int xpos;
    public int ypos;
    public float density;
    public string symbol;
    public float atomicMass;
    public string atomicNumber;
    public float atomicRadius;
    public float vanDelWaalsRadius;
    public float electronegativity;
    public string electronicConfiguration;
    public string cpkHexColor;
}

[System.Serializable]
class ElementsData
{
    public List<ElementData> elements;


    public static ElementsData FromJSON(string json)
    {
        
        return JsonUtility.FromJson<ElementsData>(json);
    }
}
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class PeriodicTableLoader : MonoBehaviour
{
    // What object to parent the instantiated elements to
    public Transform Parent;

    // Generic element prefab to instantiate at each position in the table
    public GameObject ElementPrefab;

    // How much space to put between each element prefab
    public float ElementSeperationDistance;

    public ObjectCollection Collection;

    public Material MatAlkaliMetal;
    public Material MatAlkalineEarthMetal;
    public Material MatTransitionMetal;
    public Material MatMetalloid;
    public Material MatDiatomicNonmetal;
    public Material MatPolyatomicNonmetal;
    public Material MatPostTransitionMetal;
    public Material MatNobleGas;
    public Material MatActinide;
    public Material MatLanthanide;
    public Camera Camera;
    public Transform TableParent;
	private bool isLoaded;

    void Start()
    {

        if (Collection.transform.childCount > 0)
            return;
        
        Dictionary<string, Material> typeMaterials = new Dictionary<string, Material>()
        {
            { "alkali metal", MatAlkaliMetal },
            { "alkaline earth metal", MatAlkalineEarthMetal },
            { "transition metal", MatTransitionMetal },
            { "metalloid", MatMetalloid },
            { "halogen", MatDiatomicNonmetal },
            { "nonmetal", MatPolyatomicNonmetal },
            { "metal", MatPostTransitionMetal },
            { "noble gas", MatNobleGas },
            { "actinoid", MatActinide },
            { "lanthanoid", MatLanthanide },
        };

        // Parse the elements out of the json file
        TextAsset asset = Resources.Load<TextAsset>("JSON/PeriodicTableJSON 1");
       
        List<ElementData> elements = ElementsData.FromJSON(asset.text).elements;

        // Instantiate the element prefabs in their correct locations and with correct text
        foreach (ElementData element in elements)
        {
            GameObject newElement = Instantiate<GameObject>(ElementPrefab, Parent);
            newElement.GetComponentInChildren<Element>().SetFromElementData(element, typeMaterials);
            newElement.transform.localPosition = new Vector3(element.xpos * ElementSeperationDistance - ElementSeperationDistance * 18 / 2, ElementSeperationDistance * 9 - element.ypos * ElementSeperationDistance, Collection.Radius);
            newElement.transform.localRotation = Quaternion.identity;
            //newElement.GetComponentInChildren<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        }

        // Store this configuration in the dynamic collection so we can retrieve it later
        Collection.GetComponent<ObjectCollectionDynamic>().StoreArrangement();

        Invoke("setRotation", .5f);

    }
	void Update()
	{
	

	}


    void setRotation()
    {
        TableParent.rotation = new Quaternion(0, Camera.transform.rotation.y, 0, Camera.transform.rotation.w);

        Debug.Log("pos " + Camera.transform.rotation.y + " local " + Camera.transform.localRotation.y);
    }

	
}
