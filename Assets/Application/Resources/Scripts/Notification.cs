﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class Notification : MonoBehaviour {

    #region Script's Summary
    //This script is displaying messages
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public static string _message = "";
    private Text _text;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {

        //--------- variable definitions -----------

        _text = GetComponentInChildren<Text>();

        //-------------------------------------------
    }

    private void Update()
    {
        if (_message != "")
            Show();
        else if(_text.enabled)
            Hide();
    }

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    /// <summary>
    /// display the flash message
    /// </summary>
    private void Show()
    {
        _text.enabled = true;
        _text.text = _message;
        _message = "";
    }
    
    /// <summary>
    /// hide the text field
    /// </summary>
    private void Hide()
    {
        _text.text = "";
        _text.enabled = false;
    }

    /// <summary>
    /// fade the image out
    /// </summary>
    IEnumerator FadeOut()
    {
        Image _flashImage = GetComponentInChildren<Image>();
        Color _startCol = _flashImage.color;
        Color _endCol = new Color(_flashImage.color.r, _flashImage.color.g, _flashImage.color.b, 0f);

        // Execute this loop once per frame until the timer exceeds the duration.
        float _timer = 0f;
        float _fadeDuration = 2f;
        while (_timer <= _fadeDuration)
        {
            // Set the colour based on the normalised time.
            _flashImage.color = Color.Lerp(_startCol, _endCol, _timer / _fadeDuration);

            // Increment the timer by the time between frames and return next frame.
            _timer += Time.deltaTime;
            yield return null;
        }

        _flashImage.color = new Color(_startCol.r, _startCol.g, _startCol.b, 1f);
        _flashImage.gameObject.SetActive(false);
        //_isRunning = false;
    }
    
    //----------------------------------------------------------------------

    #endregion
}
