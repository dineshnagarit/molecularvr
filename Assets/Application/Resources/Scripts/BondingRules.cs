﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class BondingRules  : MonoBehaviour {
    private BondEnergyMap energyMap;
    private Dictionary<BondEnergyMap.BondEnergyKey, int> energyDict;

    public void getSetBondEnergyValues()
    {
        energyMap = new BondEnergyMap();
        energyMap.addDataToDictionary();
        energyDict = energyMap.getDictionaryData();
       
    }

    public Vector3 CalculateForce(Transform currentAtom, AtomicInteractions atom)
    {
        // we need to have some rules here
        // http://scienceworld.wolfram.com/physics/CoulombForce.html 
        // https://socratic.org/questions/what-kind-of-charge-does-oxygen-have
        // Atoms behave differently depending upon whom they are being paired with
        // https://www.quora.com/What-is-the-electrical-charge-of-a-hydrogen-atom
        // (Atoms can also lose some of their electrons, thus becoming ions, which do have a charge.)
        // Atoms are not attracted or repelled because they are neutral but (Ions can be attracted/repelled because they are not neutral.)

        // TEMP - for now we just calculate some force whenever they are near each other but we need a better rule engine
        Vector3 dist = currentAtom.position - atom.gameObject.transform.position;
        float distSqr = dist.sqrMagnitude;

        //return (-1) * (0.001f / distSqr) * dist.normalized; // multiply by -1 to make it attract? , we need -1 if we want to attract but if there are ions and both have +ve or -ve charge then we repel //todo
        return (-1) * (0.001f / distSqr) * dist.normalized;
    }

    //public void AddSpringJoint(GameObject currentAtom, int bondNum, AtomicParameters collidedAtomParams, BondProperties bt)
    //{
    //    SpringJoint bondSJ = currentAtom.AddComponent<SpringJoint>();
    //    bt.joint = bondSJ;

    //    bondSJ.autoConfigureConnectedAnchor = false;
    //    bondSJ.connectedAnchor = new Vector3(0, 0, 0);
    //    bondSJ.anchor = new Vector3(0, 0, 0);
    //    bondSJ.connectedBody = collidedAtomParams.gameObject.GetComponent<Rigidbody>();

    //    bondSJ.spring = CalculateBondEnergy(currentAtom.name, bondNum, collidedAtomParams);
    //    bondSJ.damper = 0.5f;
    //    bondSJ.minDistance = 0.0f;
    //    bondSJ.maxDistance = 0.0f;

    //    bondSJ.breakForce = CalculateBondEnergy(currentAtom.name, bondNum, collidedAtomParams)/3;
    //    bondSJ.breakTorque = .03f;


    //}


    public void AddSpringJoint(GameObject currentAtom, int bondNum, AtomicParameters collidedAtomParams, BondProperties bt)
    {
        SpringJoint bondSJ = currentAtom.AddComponent<SpringJoint>();
        bt.joint = bondSJ;

        bondSJ.autoConfigureConnectedAnchor = false;
        bondSJ.connectedAnchor = new Vector3(0, 0, 0);
        bondSJ.anchor = new Vector3(0, 0, 0);
        bondSJ.connectedBody = collidedAtomParams.gameObject.GetComponent<Rigidbody>();

        bondSJ.spring = CalculateBondEnergy(currentAtom.name, bondNum, collidedAtomParams);
        bondSJ.damper = 0.5f;
        bondSJ.minDistance = 0.0f;
        bondSJ.maxDistance = 0.0f;

        bondSJ.breakForce = CalculateBondEnergy(currentAtom.name, bondNum, collidedAtomParams) / 3;
        bondSJ.breakTorque = .03f;


    }

    // This the method used for calculating bond energies according to the collided atoms.

    //public int CalculateBondEnergy(string currentAtomName, int bondNum, AtomicParameters collidedAtomParams)
    //{
    //    int f = 250;
    //    string name = collidedAtomParams.atomName;
    //    BondEnergyMap.BondEnergyKey key = new BondEnergyMap.BondEnergyKey();
    //    key.BondNum = bondNum;
    //    key.FirstAtom = currentAtomName;
    //    key.SecondAtom = name;
    //    if (energyDict.ContainsKey(key))
    //    {
    //        f = energyDict[key];
    //    }


    //    return f;
    //}

    public int CalculateBondEnergy(string currentAtomName, int bondNum, AtomicParameters collidedAtomParams)
    {
        int f = 250;
        string name = collidedAtomParams.atomName;
        BondEnergyMap.BondEnergyKey key = new BondEnergyMap.BondEnergyKey();
        key.BondNum = bondNum;
        key.FirstAtom = currentAtomName;
        key.SecondAtom = name;
        if (energyDict.ContainsKey(key))
        {
            f = energyDict[key];
        }


        return f;
    }


    public void setParticleEffectRadius(GameObject aura, GameObject atom)
    {
        GameObject particleSystem = aura.transform.GetChild(0).gameObject;
        Vector3 atomScale = atom.transform.localScale;
        Debug.Log("atom scale" + atomScale);
        Vector3 auraScale = new Vector3(atomScale.x + 0.04f, atomScale.y + 0.04f, atomScale.z + 0.04f);

        particleSystem.transform.localScale = auraScale;
        aura.SetActive(true);
    }
    public GameObject createMolecule(GameObject currentAtom,GameObject collidedAtom)
    {
        GameObject molecule;
        if (currentAtom.transform.parent.gameObject.name.Contains("Molecule"))
        {
            molecule = currentAtom.transform.parent.gameObject;
        }else if (collidedAtom.transform.parent.gameObject.name.Contains("Molecule"))
        {
            molecule = collidedAtom.transform.parent.gameObject;
        }
        else{
            molecule = Instantiate(Resources.Load("Prefabs/Molecule")) as GameObject;
            currentAtom.transform.SetParent(null);
            currentAtom.transform.SetParent(molecule.transform);
            collidedAtom.transform.SetParent(null);
            collidedAtom.transform.SetParent(molecule.transform);
        }

        return molecule;
    }
}
