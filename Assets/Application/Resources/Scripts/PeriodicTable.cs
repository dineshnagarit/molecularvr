﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class PeriodicTable : MonoBehaviour {

    #region Script's Summary
    //This script is 
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    //public Sprite blurImage;

    private BoxCollider[] _atomsColliders;
    private List<string> _activeAtoms = new List<string>();

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    void Start()
    {
		
        //--------- variable definitions -----------

        _activeAtoms.Add("H");
        _activeAtoms.Add("O");
        _activeAtoms.Add("C");
        _activeAtoms.Add("N");
        _activeAtoms.Add("Na");
        _activeAtoms.Add("Mg");
        _activeAtoms.Add("Cl");

        Invoke("MakeTableInteractable",2f);

        //-------------------------------------------
    }

    
	void Update () {
		
	}


    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    /// <summary>
    /// perform the neccessary modification to make the periodic table interactable
    /// </summary>
    void MakeTableInteractable()
    {
        _atomsColliders = GetComponentsInChildren<BoxCollider>();
        foreach (BoxCollider child in _atomsColliders)
        {
            string _atomName = child.GetComponent<Element>().ElementName.text;
            if (_activeAtoms.Contains(_atomName))
            {
                child.transform.tag = "TableElement";
                //child.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                GameObject parent = child.gameObject.transform.parent.gameObject;
            }
        }
    }

    //----------------------------------------------------------------------

    #endregion
}