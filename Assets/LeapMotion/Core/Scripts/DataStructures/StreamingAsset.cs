﻿using System;
using UnityEngine;

namespace Leap.Unity {

  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>
  [Serializable]
  public class StreamingAsset : StreamingFolder, ISerializationCallbackReceiver { }
}
