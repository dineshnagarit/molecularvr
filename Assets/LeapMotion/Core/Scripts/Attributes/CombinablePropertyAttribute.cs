/******************************************************************************
 * Copyright (C) Leap Motion, Inc. 2011-2017.                                 *
 * Leap Motion proprietary and  confidential.                                 *
 *                                                                            *
 * Use subject to the terms of the Leap Motion SDK Agreement available at     *
 * https://developer.leapmotion.com/sdk_agreement, or another agreement       *
 * between Leap Motion and you, your company or other organization.           *
 ******************************************************************************/

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Reflection;
using System.Collections.Generic;

namespace Leap.Unity.Attributes {

  /// <summary>
  /// 
  /// </summary>
  public interface IPropertyConstrainer {
#if UNITY_EDITOR
    void ConstrainValue(SerializedProperty property);
#endif
  }

  /// <summary>
  /// 
  /// </summary>
  public interface IPropertyDisabler {
#if UNITY_EDITOR
    bool ShouldDisable(SerializedProperty property);
#endif
  }

  /// <summary>
  /// 
  /// </summary>
  public interface IFullPropertyDrawer {
#if UNITY_EDITOR
    void DrawProperty(Rect rect, SerializedProperty property, GUIContent label);
#endif
  }

  /// <summary>
  /// 
  /// </summary>
  public interface IAdditiveDrawer {
#if UNITY_EDITOR
    float GetWidth();
    void Draw(Rect rect, SerializedProperty property);
#endif
  }

  /// <summary>
  /// 
  /// </summary>
  public interface IBeforeLabelAdditiveDrawer : IAdditiveDrawer { }
  /// <summary>
  /// 
  /// </summary>
  public interface IAfterLabelAdditiveDrawer : IAdditiveDrawer { }
  /// <summary>
  /// 
  /// </summary>
  public interface IBeforeFieldAdditiveDrawer : IAdditiveDrawer { }
  /// <summary>
  /// 
  /// </summary>
  public interface IAfterFieldAdditiveDrawer : IAdditiveDrawer { }

  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>
  public abstract class CombinablePropertyAttribute : PropertyAttribute {
    public FieldInfo fieldInfo;
    public UnityEngine.Object[] targets;

#if UNITY_EDITOR
    public virtual IEnumerable<SerializedPropertyType> SupportedTypes {
      get {
        yield break;
      }
    }

    public virtual void OnPropertyChanged(SerializedProperty property) { }
#endif
  }
}
