﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
  /// <summary>
  /// 
  /// </summary>
  /// <code>
  /// 
  /// </code>

public class HintManager : MonoBehaviour {

    #region Public Variables
    public GameObject AtomContainer;
    public GameObject hint;
    public GameObject Image;
    #endregion


    #region Private Variables
    private string hint1 = "Select an element from the periodic table";
    private string hint2 = "Select another element from the periodic table that can form stable bond with the first element";
    private string hint3 = "Bring both atoms closer to each other for them to interact with each other and form a molecule";
    #endregion
    // Use this for initialization
    void Start () {
        InvokeRepeating("showHints", 5f, 5f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void showHints()
    {
        Image.SetActive(true);
        if (AtomContainer.transform.childCount == 0)
        {
            hint.GetComponent<Text>().text = hint1;
            Invoke("disableHint", 5f);
        }else if (AtomContainer.transform.childCount == 1)
        {
            hint.GetComponent<Text>().text = hint2;
            Invoke("disableHint", 5f);
        }
        else if (AtomContainer.transform.childCount == 2)
        {
            hint.GetComponent<Text>().text = hint3;
            Invoke("disableHint", 5f);
        }
    }
    void disableHint()
    {
        Image.SetActive(false);
    }
}
